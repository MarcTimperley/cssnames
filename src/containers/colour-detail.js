import React, {Component} from 'react'
import ColourSource from '../colours'
import * as convert from '../utils/convert-colour'

class ColourDetail extends Component {
  render() {
    const colour=this.props.match.params.colour
    const colourValue = ColourSource[colour]
    if (!colourValue) return null
    let lightDarkText=convert.contrastingColour(colourValue)
    let lightDark=[]
    for (let i = 0.3; i<2.2;i=i+0.2) {
      let iRound = Math.round(i*10)/10
      lightDark.push(
        <div key={iRound} className='palette-tile stackVert'
        style={{'filter':'brightness('+iRound+')','backgroundColor':'#'+colourValue,'color':'#'+lightDarkText}}>
          <br/>brightness ({iRound})
        </div>)
    }
    let sat=[]
    for (let i = 0; i<360;i=i+15) {
      let satValue=`hsl(${convert.hexToHSL('#'+colourValue)[0]+i},${convert.hexToHSL('#'+colourValue)[1]}%,${convert.hexToHSL('#'+colourValue)[2]}%)`
      sat.push(
        <div key={'sat'+i} className='palette-tile stackVert'
        style={{'backgroundColor':satValue,'color':'#'+lightDarkText}}>
          <br/>{satValue}
        </div>)
    }

    let satPie=[]
    let step=15
    let limit=360
    for (let i=0; i<limit; i=i+step) {
      let satValue=`hsl(${(convert.hexToHSL('#'+colourValue)[0]+i)%360},${convert.hexToHSL('#'+colourValue)[1]}%,${convert.hexToHSL('#'+colourValue)[2]}%)`
      let d=getPathData(i/limit,(i+step)/limit)
      satPie.push(<path key={'satPie-'+i} d={d} fill={satValue}><title>{satValue}</title> </path>)
    }

    let alpha=[]
    for (let i = 1; i<11;i++) {
      let alphaValue=`rgba(${convert.hexToRGBArray(colourValue).toString()},${i/10})`
      alpha.push(
        <div key={'alpha'+i/10} className='palette-tile stackVert'
        style={{'backgroundColor':alphaValue,'color':'#'+lightDarkText}}>
          <br/>{alphaValue}
        </div>)
    }
    let rgbValue = convert.hexToRGBArray(colourValue).toString()
    console.log(convert.hexToHSLString('#'+colourValue))
    return (
      <div>
        <div className="colour_detail">
          Congratulations for choosing {colour}!<br/>
          RGB: {rgbValue}<br/>
          HSL: {convert.hexToHSLString('#'+colourValue)}
        </div>

        <div className='palette-tile' style={{'backgroundColor':'#'+colourValue,'color':'#'+convert.contrastingColour(colourValue)}}>
          {colour}<br/>
          #{colourValue}
        </div>
        <div className="flex-row">
          <div className="lightDark">
            <div className="detailTitle">
              Brightness
            </div>
            {lightDark}
          </div>
          <div className="satPie">
          <div className="detailTitle">
            Hue
          </div>
            <svg key='satPie-svg' className='satPie-svg' viewBox='-1 -1 2 2' style={{'transform': 'rotate(-0.25turn)'}}>
              {satPie}
            </svg>
          </div>
          <div className="alpha">
            <div className="detailTitle">
              Transparency
            </div>
            {alpha}
          </div>
        </div>
      </div>
    )
  }
}

function getCoordinatesForPercent(percent) {
  const x = Math.cos(2 * Math.PI * percent)
  const y = Math.sin(2 * Math.PI * percent)

  return [x, y];
}

function getPathData(start, percent) {

const startX = getCoordinatesForPercent(start)[0]
const startY = getCoordinatesForPercent(start)[1]
const endX = getCoordinatesForPercent(percent)[0]
const endY = getCoordinatesForPercent(percent)[1]

const largeArcFlag = percent-start > .5 ? 1 : 0;
let d=[
  `M ${startX} ${startY}`,
  `A 1 1 0 ${largeArcFlag} 1 ${endX} ${endY}`,
  `L 0 0`,
].join(' ')
return d
}

export default ColourDetail
