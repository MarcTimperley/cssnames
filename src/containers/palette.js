import React, {Component} from 'react'
import Switch from "react-switch"
import ColourSource from '../colours'
import {Link} from 'react-router-dom'
import * as convert from '../utils/convert-colour'

class Palette extends Component {
    constructor(props) {
      super(props);
      this.state = {search: '',
      sortAsc:false};

      this.handleChange = this.handleChange.bind(this);
      this.handleChangeSort = this.handleChangeSort.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChangeSort(sortAsc) {
        this.setState({sortAsc})
      }

    handleChange(event) {
      this.setState({search: event.target.value});
    }

    handleSubmit(event) {
      alert('A name was submitted: ' + this.state.search);
      event.preventDefault();
    }

  render() {
    let colours=[]
    this.state.sortAsc? colours = Object.entries(ColourSource).sort() : colours = Object.entries(ColourSource)
    let display=[]
    for (let [key,value] of colours){
      if(key.toLowerCase().match(this.state.search.toLowerCase())) {
        display.push(
          <Link key={`link${key}`} to={`/colour/${key}`}>
            <div key={key}
            className='palette-tile'
            style={{'backgroundColor':'#'+value,'color':'#'+convert.contrastingColour(value)}}>
              {key}<br/>#{value}
            </div>
          </Link>
        )
      }
    }
      return (
      <div>
        <label>
        <input className='palette-search' id='palette-search' placeholder='Search by name' value={this.state.search} onChange={this.handleChange}></input>
          <span>Sort by name</span>
          <Switch
            onChange={this.handleChangeSort}
            checked={this.state.sortAsc}
            className="react-switch"
          />
        </label>
        <div className='palette-tile-area'>

          {display}

        </div>
      </div>
    )
  }
}


export default Palette
