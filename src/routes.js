import React, {Component} from 'react'

import Palette from './containers/palette'
import ColourDetail from './containers/colour-detail'
import createBrowserHistory from './utils/history'

import {Router, Route, Switch} from 'react-router'

class Routes extends Component {
  render() {
    return (
      <div>
      <Router history={createBrowserHistory}>
          <div className="header">
          </div>
          <div className="content">
          <Switch>
            <Route exact={true} path="/" render={() => <Palette/>}/>
            <Route path="/colour/:colour" render={(props) => <ColourDetail {...props}/>}/>
          </Switch>
          </div>

      </Router>
    </div>
  )
}
}

export default Routes
